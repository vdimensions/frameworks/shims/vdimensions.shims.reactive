﻿#if !UNITY_WEBGL
using System;
using System.IO;
using System.Net;
using VDimensions.Shims;

namespace UniRx
{
    public static class WebRequestExtensions
    {
        static IObservable<TResult> AbortableDeferredAsyncRequest<TResult>(Func<AsyncCallback, object, IAsyncResult> begin, Func<IAsyncResult, TResult> end, WebRequest request)
        {
            var result = System.Reactive.Linq.Observable.Create<TResult>(observer =>
            {
                var isCompleted = -1;
                var subscription = System.Reactive.Linq.Observable.FromAsyncPattern<TResult>(
                                             begin,
                                             ar =>
                                             {
                                                 try
                                                 {
                                                     WebGLInterlocked.Increment(ref isCompleted);
                                                     return end(ar);
                                                 }
                                                 catch (WebException ex)
                                                 {
                                                     if (ex.Status == WebExceptionStatus.RequestCanceled)
                                                     {
                                                         return default(TResult);
                                                     }

                                                     throw;
                                                 }
                                             })()
                                         .Subscribe(observer);
                return Disposable.Create(() =>
                {
                    if (WebGLInterlocked.Increment(ref isCompleted) == 0)
                    {
                        subscription.Dispose();
                        request.Abort();
                    }
                });
            });

            return result;
        }

        public static IObservable<WebResponse> GetResponseAsObservable(this WebRequest request)
        {
            return AbortableDeferredAsyncRequest<WebResponse>(request.BeginGetResponse, request.EndGetResponse, request);
        }

        public static IObservable<HttpWebResponse> GetResponseAsObservable(this HttpWebRequest request)
        {
            return AbortableDeferredAsyncRequest<HttpWebResponse>(request.BeginGetResponse, ar => (HttpWebResponse)request.EndGetResponse(ar), request);
        }

        public static IObservable<Stream> GetRequestStreamAsObservable(this WebRequest request)
        {
            return AbortableDeferredAsyncRequest<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, request);
        }
    }
}
#endif