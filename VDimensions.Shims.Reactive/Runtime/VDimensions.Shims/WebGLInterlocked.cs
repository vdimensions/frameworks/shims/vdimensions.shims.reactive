﻿using System.Runtime.CompilerServices;
using System.Threading;

namespace VDimensions.Shims
{
    internal static class WebGLInterlocked
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int Increment(ref int value)
        {
            #if !UNITY_WEBGL
            return Interlocked.Increment(ref value);
            #else
            return ++value;
            #endif
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long Increment(ref long value)
        {
            #if !UNITY_WEBGL
            return Interlocked.Increment(ref value);
            #else
            return ++value;
            #endif
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T Exchange<T>(ref T location, T value) where T: class
        {
            #if !UNITY_WEBGL
            return Interlocked.Exchange(ref location, value);
            #else
            var copy = location;
            location = value;
            return copy;
            #endif
        }
    }
}