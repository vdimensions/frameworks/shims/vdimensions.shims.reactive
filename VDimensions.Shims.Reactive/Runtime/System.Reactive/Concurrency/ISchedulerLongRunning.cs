﻿using System.Reactive.Disposables;
using UniRx;

namespace System.Reactive.Concurrency
{
    public interface ISchedulerLongRunning
    {
        IDisposable ScheduleLongRunning(Action<ICancelable> action);
    }
}