﻿using System.Reactive.Disposables;
using UniRx;

namespace System.Reactive.Concurrency
{
    public interface ISchedulerQueueing
    {
        void ScheduleQueueing<T>(ICancelable cancel, T state, Action<T> action);
    }
}