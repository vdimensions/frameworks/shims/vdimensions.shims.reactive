﻿namespace System.Reactive.Concurrency
{
    public interface ISchedulerPeriodic
    {
        IDisposable SchedulePeriodic(TimeSpan period, Action action);
    }
}