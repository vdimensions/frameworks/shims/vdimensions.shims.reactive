﻿using System;
using System.Reactive.Disposables;

namespace UniRx
{
    public partial class Observable
    {
        static IObservable<T> AddRef<T>(IObservable<T> xs, RefCountDisposable r)
        {
            return System.Reactive.Linq.Observable.Create<T>((IObserver<T> observer) => new CompositeDisposable(new IDisposable[]
            {
                r.GetDisposable(),
                xs.Subscribe(observer)
            }));
        }
    }
}