﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;

#if CSHARP_7_OR_LATER || (UNITY_2018_3_OR_NEWER && (NET_STANDARD_2_0 || NET_4_6))
using System.Threading.Tasks;
using UniRx.InternalUtil;
#endif

namespace UniRx
{
    public static partial class AsyncReactiveCommandExtensions
    {
        public static AsyncReactiveCommand ToAsyncReactiveCommand(this IReactiveProperty<bool> sharedCanExecuteSource)
        {
            return new AsyncReactiveCommand(sharedCanExecuteSource);
        }

        public static AsyncReactiveCommand<T> ToAsyncReactiveCommand<T>(this IReactiveProperty<bool> sharedCanExecuteSource)
        {
            return new AsyncReactiveCommand<T>(sharedCanExecuteSource);
        }

#if CSHARP_7_OR_LATER || (UNITY_2018_3_OR_NEWER && (NET_STANDARD_2_0 || NET_4_6))

        static readonly Action<object> Callback = CancelCallback;

        static void CancelCallback(object state)
        {
            var tuple = (Tuple<ICancellableTaskCompletionSource, IDisposable>)state;
            tuple.Item2.Dispose();
            tuple.Item1.TrySetCanceled();
        }

        public static Task<T> WaitUntilExecuteAsync<T>(this IAsyncReactiveCommand<T> source, CancellationToken cancellationToken = default(CancellationToken))
        {
            var tcs = new CancellableTaskCompletionSource<T>();

            var subscription = source.Subscribe(x => { tcs.TrySetResult(x); return System.Reactive.Linq.Observable.ReturnUnit(); });
            cancellationToken.Register(Callback, Tuple.Create(tcs, subscription), false);

            return tcs.Task;
        }

        public static System.Runtime.CompilerServices.TaskAwaiter<T> GetAwaiter<T>(this IAsyncReactiveCommand<T> command)
        {
            return command.WaitUntilExecuteAsync(CancellationToken.None).GetAwaiter();
        }

#endif
    }
}
