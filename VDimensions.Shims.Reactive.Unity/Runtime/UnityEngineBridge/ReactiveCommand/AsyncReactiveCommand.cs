﻿using System;
using System.Reactive.Linq;
using UniRx.InternalUtil;

namespace UniRx
{
    /// <summary>
    /// Variation of ReactiveCommand, canExecute is changed when executing command then CanExecute = false after CanExecute = true.
    /// </summary>
    public class AsyncReactiveCommand<T> : IAsyncReactiveCommand<T>
    {
        ImmutableList<Func<T, IObservable<Unit>>> asyncActions = ImmutableList<Func<T, IObservable<Unit>>>.Empty;

        readonly object gate = new object();
        readonly IReactiveProperty<bool> canExecuteSource;
        readonly IReadOnlyReactiveProperty<bool> canExecute;

        public IReadOnlyReactiveProperty<bool> CanExecute => canExecute;

        public bool IsDisposed { get; private set; }

        /// <summary>
        /// CanExecute is automatically changed when executing to false and finished to true.
        /// </summary>
        public AsyncReactiveCommand()
        {
            canExecuteSource = new ReactiveProperty<bool>(true);
            canExecute = canExecuteSource;
        }

        /// <summary>
        /// CanExecute is automatically changed when executing to false and finished to true.
        /// </summary>
        public AsyncReactiveCommand(IObservable<bool> canExecuteSource)
        {
            this.canExecuteSource = new ReactiveProperty<bool>(true);
            this.canExecute = this.canExecuteSource.CombineLatest(canExecuteSource, (x, y) => x && y).ToReactiveProperty();
        }

        /// <summary>
        /// CanExecute is automatically changed when executing to false and finished to true.
        /// The source is shared between other AsyncReactiveCommand.
        /// </summary>
        public AsyncReactiveCommand(IReactiveProperty<bool> sharedCanExecute)
        {
            this.canExecuteSource = sharedCanExecute;
            this.canExecute = sharedCanExecute;
        }

        /// <summary>Push parameter to subscribers when CanExecute.</summary>
        public IDisposable Execute(T parameter)
        {
            if (canExecute.Value)
            {
                canExecuteSource.Value = false;
                var a = asyncActions.Data;
                if (a.Length == 1)
                {
                    try
                    {
                        var asyncState = a[0].Invoke(parameter) ?? System.Reactive.Linq.Observable.ReturnUnit();
                        return asyncState.Finally(() => canExecuteSource.Value = true).Subscribe();
                    }
                    catch
                    {
                        canExecuteSource.Value = true;
                        throw;
                    }
                }
                else
                {
                    var xs = new IObservable<Unit>[a.Length];
                    try
                    {
                        for (int i = 0; i < a.Length; i++)
                        {
                            xs[i] = a[i].Invoke(parameter) ?? System.Reactive.Linq.Observable.ReturnUnit();
                        }
                    }
                    catch
                    {
                        canExecuteSource.Value = true;
                        throw;
                    }

                    return System.Reactive.Linq.Observable.WhenAll(xs).Finally(() => canExecuteSource.Value = true).Subscribe();
                }
            }
            else
            {
                return Disposable.Empty;
            }
        }

        /// <summary>Subscribe execute.</summary>
        public IDisposable Subscribe(Func<T, IObservable<Unit>> asyncAction)
        {
            lock (gate)
            {
                asyncActions = asyncActions.Add(asyncAction);
            }

            return new Subscription(this, asyncAction);
        }

        /// <summary>
        /// Stop all subscription and lock CanExecute is false.
        /// </summary>
        public void Dispose()
        {
            if (IsDisposed) return;

            IsDisposed = true;
            asyncActions = UniRx.InternalUtil.ImmutableList<Func<T, IObservable<Unit>>>.Empty;
        }
        class Subscription : IDisposable
        {
            readonly AsyncReactiveCommand<T> parent;
            readonly Func<T, IObservable<Unit>> asyncAction;

            public Subscription(AsyncReactiveCommand<T> parent, Func<T, IObservable<Unit>> asyncAction)
            {
                this.parent = parent;
                this.asyncAction = asyncAction;
            }

            public void Dispose()
            {
                lock (parent.gate)
                {
                    parent.asyncActions = parent.asyncActions.Remove(asyncAction);
                }
            }
        }
    }

    /// <summary>
    /// Variation of ReactiveCommand, when executing command then CanExecute = false after CanExecute = true.
    /// </summary>
    public class AsyncReactiveCommand : AsyncReactiveCommand<Unit>
    {
        /// <summary>
        /// CanExecute is automatically changed when executing to false and finished to true.
        /// </summary>
        public AsyncReactiveCommand()
            : base()
        {
        }

        /// <summary>
        /// CanExecute is automatically changed when executing to false and finished to true.
        /// </summary>
        public AsyncReactiveCommand(IObservable<bool> canExecuteSource)
            : base(canExecuteSource)
        {
        }

        /// <summary>
        /// CanExecute is automatically changed when executing to false and finished to true.
        /// The source is shared between other AsyncReactiveCommand.
        /// </summary>
        public AsyncReactiveCommand(IReactiveProperty<bool> sharedCanExecute)
            : base(sharedCanExecute)
        {
        }

        public IDisposable Execute()
        {
            return base.Execute(Unit.Default);
        }
    }
}