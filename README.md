# VDimensions.Shims.Reactive

This project is aimed as a shim for the [dotnet/reactive](https://github.com/dotnet/reactive) (Reactive Extensions) a.k.a. `System.Reactive` for use in the Unity Game Engine. The implementation is based on the [UniRx](https://github.com/neuecc/UniRx) project, which is an excellent replacement for System.Reactive in most cases. However, for our own use case with VDimensions, the original UniRx project is not suitable as-is because:  

* Our projects rely on a framework that is split into a generic and engine-specific codebases. We need to be able to interact with observables and subjects
from the generic codebase without that codebase to make explicit references to Unity code.
* For the above reasons, we need to make it transparent for our codebase whether we use the actual `System.Reactive` or a flavour that is Unity-Specific. Hence, the common Rx types should be accessible from the System namespace just as in the original implementation.
* We need to make any code that requires or exposes Unity-specific apis reside in a separate assembly definition. That way we can prevent referencing such code in our generic codebase, but use it in the Unity-specific codebase. Ideally, we also split such code into separate upm packages.
* We prefer upm packages for library distribution over the Asset Store format (mostly because of the direct git support and easier version management/installation).  

With this project, we will attempt to cover the above criteria by adapting the original solution from [UniRx](https://github.com/neuecc/UniRx). Therefore we will use the same [open-source license](LICENSE) as UniRx. Additionally, a reference to the UniRx codebase is kept as a submodule for convenience with keeping up-to-date with the original project, as well as to ease contributions back to UniRx.

## Using in Unity

The easiest way is to install the packages contained in this repository. To do this, add the following lines under the `dependencies` section of the `manifest.json` file in your Unity project's `Packages` directory:

`
"com.vdimensions.shims.reactive": "https://gitlab.com/vdimensions/frameworks/shims/vdimensions.shims.reactive?path=src/VDimensions.Shims.Reactive#master",
"com.vdimensions.shims.reactive.unity": "https://gitlab.com/vdimensions/frameworks/shims/vdimensions.shims.reactive?path=src/VDimensions.Shims.Reactive.Unity#master"
`

If you applied this correctly, your `manifest.json` should look similar to this:

```json
{
    "dependencies": {
        "com.vdimensions.shims.reactive": "https://gitlab.com/vdimensions/frameworks/shims/vdimensions.shims.reactive?path=src/VDimensions.Shims.Reactive#master",
        "com.vdimensions.shims.reactive.unity": "https://gitlab.com/vdimensions/frameworks/shims/vdimensions.shims.reactive?path=src/VDimensions.Shims.Reactive.Unity#master"
    }
}
```

**NOTE**

While this codebase is based on UniRx, it wont work if UniRx is already installed in your project. Remove UniRx from your project first beforehand.

